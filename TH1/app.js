const express = require('express');
const routerUser = require('./Routes/User');
const routerPosts = require('./Routes/Posts');

const app = express();

const http = require('http').Server(app);

app.use(express.json());
app.use (express.urlencoded({extended: true}))


app.use('/user', routerUser);
app.use('/posts', routerPosts);

http.listen(4000);