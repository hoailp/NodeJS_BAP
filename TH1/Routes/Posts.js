const express = require('express');
const router = express.Router();

// Get list posts
router.get('/', (req, res, next) => {
    res.status(200).json({
        success: 'Get list post!'
    })
})

// Get posts
router.get('/:id', (req, res, next) => {
    const { id } = req.params;
    res.status(200).json({
        success: 'Get post có id: '+id
    })
})

// Post 1 posts
router.post('/', (req, res, next) => {
    const { sub, content } = req.body;
    res.status(201).json({
        success: 'Post thanh cong',
        subject: sub,
        Content: content
    })
})

// Put 1 posts
router.put('/:id', (req, res, next) => {
    const { id } = req.params;
    const { sub } = req.body;
    console.log(id, sub);

    res.status(200).json({
        success: 'Update thành công tiêu đề posts có id: '+id,
        Subject: sub
    })
})

// Xóa 1 posts
router.delete('/:id', (req, res, next) => {
    const { id } = req.params;

    res.status(201).json({
        success: 'Dã xóa thành công post có id: '+id
    })
})

module.exports = router;