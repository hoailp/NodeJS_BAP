const express = require('express');
const router = express.Router();

// Get list User
router.get('/', (req, res, next) => {
    console.log('Get list user!');
    res.status(200).json({
        success: 'Get list User'
    })
})

// Get User ID
router.get('/:id', (req, res, next) => {
    const { id } = req.params;
    console.log('Get user id: ', id);
    res.status(200).json({
        success: "Get user: "+id
    })
})

// add user
router.post('/', (req, res, next) => {
    const { name, pass } = req.body;
    console.log('Post user: name: '+name+', pass: '+pass);
    res.status(201).json({
        success: 'Post user thành công: name = '+name+' & pass: '+pass
    })
})

// update
router.put('/:id', (req, res, nẽt) => {
    const { pass } = req.body;
    const { id } = req.params;
    console.log('Update pass user co id: '+id);
    res.status(200).json({
        success: 'Put pass thành công id: '+ id +'. Pass mới là: '+pass
    })
})

router.delete('/:id', (req, res, next) => {
    const { id } = req.params;
    console.log('Xoa user co id: ', id);
    res.status(200).json({
        success: 'Xoá thành công user có id là: '+id
    })
})

module.exports = router