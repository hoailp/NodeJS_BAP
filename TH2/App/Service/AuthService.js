const usersModel = require('../Models/UsersModel');
const tokensModel = require('../Models/TokensModel');
const Bcrypt = require('../../Helper/BcryptPassword');
// const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


class AuthService {
    constructor() {
        this.usersModel = usersModel;
        this.tokensModel = tokensModel;
    }
    
    async register(body) {
        try {            
            console.log(body.username, body.password);
            
            // Step 2
            if(!body.username || !body.password || !body.name) {
                return {
                    message: 'username or password or name is required',
                    data: null
                }
            }
    
            // Step 3
            const user = await this.usersModel.query()
            .where('username', body.username)
            .first();
    
            // Step 4
            if(user) {
                return{
                    message: 'user is exit',
                    data: null
                }
            }
            
            // Step 5
            const password = Bcrypt.hash(body.password, 10);
                    
            const dataInsert = {
                username: body.username,
                password,
                name: body.name,
            }
    
            const userInserted = await this.usersModel.query().insert(dataInsert);
            console.log('Register success user: ',userInserted);
            
            // Step 6
    
            const token = jwt.sign({
                id: userInserted.id,
                timestamp: new Date().getTime()
            }, Env.APP_KEY);
            
            const dataTokenInsert = {
                user_id: userInserted.id,
                token: token,
                status: 1
            }
    
            await this.tokensModel.query().insert(dataTokenInsert);
    
            console.log('Token: ', dataTokenInsert);
    
            return{
                message: 'Register Success!!',
                data: token
            }
        } catch (error) {
            
        }
    }
}

module.exports = new AuthService();
