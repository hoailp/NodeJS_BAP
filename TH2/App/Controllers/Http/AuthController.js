
// const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const authService = require('../../Service/AuthService')

class AuthController {
    constructor() {
        // super();
        
        this.authService = authService;
    }

    async register({ req, res, next }){
        // Step 1
        const { body } = req;

        const result = await this.authService.register(body);
        console.log(result);

        return res.json(result);
        
    }

    async login({ req, res, next }) {
        const { body } = req;

        if(!body.username || !body.password) {
            return res.json({
                message: 'username or password is required',
                data: null
            })
        }

        const user = await this.usersModel.query()
        .where('username', body.username)
        .first();

        if(!user) {
            return res.json({
                message: 'username is wrong!!!',
                data: null
            })
        }
        console.log('Found user: ', user);
        if(!Bcrypt.compare(body.password, user.password)) {
            return res.json({
                message: 'Password is wrong!!!',
                data: null
            })
        }

        const token = jwt.sign({
            id: user.id,
            timestamp: new Date().getTime()
        }, Env.APP_KEY);

        const dataTokenInsert = {
            user_id: user.id,
            token: token,
            status: 1
        }
        console.log(dataTokenInsert);
        
        await this.tokensModel.query()
            .insert(dataTokenInsert)

        return res.json({
            message: 'Login success',
            data: null,
            token
        })
    }

    async logout({ req, res, next }) {
        const { body } = req;
        if(!body.username) {
            return res.json({
                message: 'Username isn\'t require!!!',
                data: null
            })
        }
        const user = await this.usersModel.query()
        .where('username', body.username)
        .first();
        
        if(!user) {
            return res.json({
                message: 'Username wrong!!!',
                data: null
            })
        }
        const token  = req.headers.authorization;
        console.log(token);
        await this.tokensModel
        .query()
        .where({
            'user_id': user.id,
            'token' : token
        })
        .del();
        return res.json({
            message: 'Logout success',
            data: null
        })
        
    }
}

module.exports = new AuthController();
