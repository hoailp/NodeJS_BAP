const express = require('express');
require('dotenv').config();

global.Env = process.env;

const app = express();

const http = require('http').Server(app);

app.use(express.json());

http.listen(Env.PORT, () => {
    console.log(`Server run at port: ${Env.PORT}`);
});

const io = require('socket.io')(http, {
    pingTimeout: 30000,
    pingInterval: 60000
})

io.on('connect', (socket) => {
    console.log('client connect with id: '+socket.id);
    socket.on('message', (data) => {
        console.log(data);
        socket.emit('client-connect', socket.id);
    })
    socket.on('disconnect',() => {
        console.log('DISCONNECT');
        
    })
})

module.exports = app;
