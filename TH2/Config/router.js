const app = require('./app');

const authorization = require('../Routes/authorization');
const teamRouter = require('../Routes/Team');
const userRouter = require('../Routes/User')

const apiPrefix = '/api/v1';

app.use(`${apiPrefix}/auth`, authorization);
app.use(`${apiPrefix}/team`, teamRouter);
app.use(`${apiPrefix}/user`, userRouter);
